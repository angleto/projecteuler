#!/usr/bin/env bash

find . -type f -name "*.hs" | while read line
do
	echo ">>>>Building: ${line}"
	ghc -O3 -v3 ${line}
	RES=${?}

	if [[ ${RES} -ne 0 ]]; then
		echo "<<<<Error: building ${line}"
	else
		echo "<<<<Completed building of: ${line}"
	fi
done

