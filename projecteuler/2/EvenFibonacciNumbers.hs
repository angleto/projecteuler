import System.Environment

-- http://mathworld.wolfram.com/BinetsFibonacciNumberFormula.html
fib n = round (((1+sqrt(5))**n-(1-sqrt(5))**n)/(2**n*sqrt(5)))

fib_rec :: Integer -> Integer
fib_rec 0 = 1
fib_rec 1 = 1
fib_rec n = fib_rec (n-1) + fib_rec (n-2)

calc_fib_sum start_fib max_val fib_func =
	let fib_val = fib_func start_fib 
	in
		if fib_val <= max_val
		then
			let addendum = if mod fib_val 2 == 0 then fib_val else 0
			in addendum + calc_fib_sum (start_fib + 1) max_val fib_func
		else 0

main = do
	let v = calc_fib_sum 2 4000000 fib
	print "Binet Fib. formula"
	print v

