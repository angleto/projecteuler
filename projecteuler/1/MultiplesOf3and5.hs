import System.Environment

calc_mult start_val max_val mod_vals=
	if start_val < max_val
	then
		if any (\x -> mod start_val x == 0) mod_vals
		then
			start_val + calc_mult (start_val + 1) max_val mod_vals
		else 
			calc_mult (start_val + 1) max_val mod_vals
	else 0

main = do
	let v = calc_mult 1 1000 [3,5]
	print "MultiplesOf3and5"
	print v 

